# Kubernetes



## Requisitos

Para esse lab você vai precisar apenas de 3 ferramentas

**[Docker](https://docs.docker.com/engine/install/ubuntu/)** 

**[Kind](https://kind.sigs.k8s.io/docs/user/quick-start/)**

**[kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)**

## Criando um cluster com kind

```
kind create  cluster --config kind-config.yaml
```

## Definições

**Pod**: Um Pod do Kubernetes é um conjunto de um ou mais containers Linux, sendo a menor unidade de uma aplicação Kubernetes. Os Pods são compostos por um container nos casos de uso mais comuns ou por vários containers fortemente acoplados em cenários mais avançados. Os containers de um Pod compartilham os mesmos recursos de computação e a mesma rede local. Isso permite que os containers se comuniquem uns com os outros como se compartilhassem o mesmo hardware físico, enquanto mantêm um certo nível de isolamento.


**Deployment**: Um Deployment no Kubernetes é responsável por criar e atualizar instâncias de seu aplicativo. Quando você cria um Deployment, precisa especificar a imagem do contêiner para seu aplicativo e o número de réplicas que deseja executar. Você pode alterar essas informações posteriormente, atualizando seu Deployment. O Deployment monitora continuamente o estado dos Pods e, se algum Pod falhar, o Deployment iniciará um novo Pod em seu lugar para manter o número desejado de réplicas.


**Service**: Um Service no Kubernetes é uma abstração que define um conjunto lógico de Pods e permite a exposição do tráfego externo, o balanceamento de carga e a descoberta de serviços para esses Pods. Os Services atuam como uma abstração para encaminhar o tráfego para os Pods corretos, com base em seletores.



## Comandos importantes


* kubectl get pods: Lista todos os pods.

* kubectl get services: Lista todos os serviços.

* kubectl describe pod <nome-pod>: Fornece detalhes sobre um pod específico.

* kubectl describe service <nome>: Fornece detalhes sobre um serviço específico.

* kubectl create -f <nome-arquivo-yml>: Cria um pod, deployment ou service a partir de um arquivo yml.

* kubectl delete -f <nome-arquivo-yml>: Remove um pod, deployment ou service a partir de um arquivo yml.

* kubectl delete pod <nome-pod>: Remove um pod específico.

* kubectl delete deployment <nome-deployment>: Remove um deployment específico.

* kubectl delete service <nome-service>: Remove um serviço específico.






